import { configureStore } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import {decrypt} from 'n-krypta';
import VerifyJWT from "../Auth/VerifyJWT";
import GenerateJWT from "../Auth/GenerateJWT";
import DecodeJWT from "../Auth/DecodeJWT";

const jwtSlice = createSlice({
    name: 'jwt',
    initialState: {tokenVerify: false, name: "Guest", id: "", token: localStorage.getItem("token")? localStorage.getItem("token"):"", secretkey: 'ovvykUwuRd4k**q!A%L^!rA@nJ#3SpK3pGG4NEp7pF5*y3R^Zh'},
    reducers: {
        tokenGenerate(state, action){
            // console.log(action.payload);
            GenerateJWT(action.payload);
        },
        tokenVerify(state){
           const decr = decrypt(state.token, state.secretkey);
           state.tokenVerify=VerifyJWT(decr);
        },
        tokenDecodeName(state){
            state.name = state.tokenVerify ? DecodeJWT().name : state.name
        },
        tokenDecodeId(state){
            state.id = state.tokenVerify ? DecodeJWT().id : state.id
        }
    }
});

const uiSlice = createSlice({
    name: 'ui',
    initialState: {notification: null, id: null},
    reducers: {
        showNotification(state, action){
            state.notification = action.payload;
        },
        editBlog(state, action){
            state.id = action.payload;
        }
    }
});

const store = configureStore({
    reducer: {jwt: jwtSlice.reducer, ui: uiSlice.reducer}
})

export const jwtActions = jwtSlice.actions;
export const uiActions = uiSlice.actions;
export default store;