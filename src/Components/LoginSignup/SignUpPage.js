import { Link, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import {encrypt} from 'n-krypta';
import { useDispatch, useSelector } from 'react-redux';
import { uiActions } from '../../Store/indexStore';

const SignUpPage = () => {
    const [name, setName] = useState('');
    const [username, setUserName] = useState('');
    const [plainpassword, setPlainPassword] = useState('');
    const [confirmpass, setConfirmPass] = useState('');
    const [message, setMessage] = useState(false);
    const [users, setUsers] = useState([]);
    const dispatch = useDispatch();
    const notification = useSelector(state => state.ui.notification);
    const navigate = useNavigate();
    

    useEffect(() => {
        fetch('http://localhost:8000/users')
          .then(res => res.json())
          .then(data => {
            setUsers(data);
          })
          .catch(error => {
            console.error('Error fetching users:', error);
          });
      }, []);

    const addUser = (e) => {
        e.preventDefault();
        const userExists = users.some(user => user.id === username);
        if (userExists) {
            setMessage(true);
            dispatch(uiActions.showNotification("Username already exists"));
        } 
        else if(plainpassword.length<6){
            setMessage(true);
            dispatch(uiActions.showNotification("Password length must be greater than 6"));
        } 
        else if (plainpassword !== confirmpass) {
            setMessage(true);
            dispatch(uiActions.showNotification("Password does not match"));
            
        } 
        else {
                const secretkey = 'ovvykUwuRd4k**q!A%L^!rA@nJ#3SpK3pGG4NEp7pF5*y3R^Zh'
                const encryptpassword = plainpassword
                const password = encrypt(encryptpassword, secretkey);
                const id = username;
                const user = { name, id, password };
    
                fetch('http://localhost:8000/users', {
                    method: 'POST',
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(user)
                }).then(res => {
                    if(!res.ok){
                        throw Error('Error in server! Failed creating account');
                    }
                    return res.json();
                })
                .then(() => {
                    navigate("/loginsignup/login");
                })
                .catch(err => {
                    setMessage(true);
                    dispatch(uiActions.showNotification(err.message));
                })
        }
     }

    return ( 
        <>
        { message && <div className="notification">
            {notification}
            </div> }
            <div className="login-signup-page">
                <h2>SignUp Page</h2>
                <form onSubmit={addUser}>
                    <label>Name:</label>
                    <input type="text" required value={name}
                    onChange={(e) => setName(e.target.value)}/>
                    <label>UserName:</label>
                    <input type="text" required value={username}
                    onChange={(e) => setUserName(e.target.value)}/>
                    <label>Password:</label>
                    <input type="password" required value={plainpassword}
                    onChange={(e) => setPlainPassword(e.target.value)}/>
                    <label>Confirm Password:</label>
                    <input type="password" required value={confirmpass}
                    onChange={(e) => setConfirmPass(e.target.value)}/>
                    <button>SignUp</button>
                </form>
                <Link to=".." relative="path"  style={{color: 'white'}}>Go back to login</Link>
            </div>
        </>
     );
}
 
export default SignUpPage;