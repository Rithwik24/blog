import { useState } from 'react';
import { Link } from 'react-router-dom'
import {decrypt} from 'n-krypta';
import { useDispatch, useSelector } from 'react-redux';
import { jwtActions, uiActions } from '../../Store/indexStore';

const LoginPage = () => {
  const [id, setID] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState(false);
  const notification = useSelector(state => state.ui.notification);
  const dispatch = useDispatch();

     const loginUser =  (e) => {
        e.preventDefault();
          // console.log(VerifyJWT(token));
          const secretkey = 'ovvykUwuRd4k**q!A%L^!rA@nJ#3SpK3pGG4NEp7pF5*y3R^Zh';
          fetch('http://localhost:8000/users/'+id)
          .then(res => {
            if(res.status === 404){
                setMessage("Username doesnot exist")
            }
            return res.json();
          })
          .then(data => {
            if (data.id !== id){
                setMessage(true);
                dispatch(uiActions.showNotification("Username doesnot exist"));
            }
            else if(decrypt(data.password, secretkey) !== password){
                setMessage(true);
                dispatch(uiActions.showNotification("Password incorrect"));
            } 
            else{
                // setuser(id);
                // login(id);
                // GenerateJWT({id:id, name: data.name});
                dispatch(jwtActions.tokenGenerate({id:id, name: data.name}));
                window.location.reload();
                
            }
          })
          .catch(error => {
            console.error('Error fetching users:', error);
          });
        
        }
     
    return (
      <>
       { message && <div className="notification">
          {notification}
        </div> }
        <div className="login-signup-page">
            <h2>Login Page</h2>
            <form onSubmit={loginUser}>
                <label>UserName:</label>
                <input type="text" required value={id}
                onChange={(e) => setID(e.target.value)}/>
                <label>Password:</label>
                <input type="password" required value={password}
                onChange={(e) => setPassword(e.target.value)}/>
                <button>Login</button>
            </form>
            {/* <Link to='/signup' style={{color: 'white'}}>Create your Blog account</Link> */}
            <Link to=".." relative="path"  style={{color: 'white'}}>Go back to signup</Link>
        </div>
      </>
     );
}
 
export default LoginPage;