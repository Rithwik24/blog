import { Link } from "react-router-dom";
// import LoginPage from "./LoginPage";
// import SignUpPage from "./SignUpPage";

const LoginSignup = () => {
    return ( 
        <div className="loginsignup">
            <Link to="/loginsignup/login" style={{marginRight: '80px'}}>Existing User, login!</Link>
            <Link to='/loginsignup/signup' style={{marginLeft: '80px'}}>New user, sign up now!</Link>
        </div>
     );
}
 
export default LoginSignup;