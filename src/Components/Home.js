import { Link } from "react-router-dom";

const Home = () => {
    return (
        <>
            <div className="home">
                <h1>The Blog, where all bloggers meet!</h1>
            </div>
            <div style={{textAlign: 'center', marginTop: '20px', fontSize: '30px'}}>
                <Link to='/blog' style={{textDecoration: 'none'}}>Click here to check blogs</Link>
            </div>
        </>
    );
}
 
export default Home;