import { Outlet } from "react-router-dom";
import Navbar from "./Navbar";

const Root = () => {
    const url = "https://img.freepik.com/free-vector/prickly-juniper-branch-beige-gray-minimal-background_53876-113047.jpg?w=2000&t=st=1688472415~exp=1688473015~hmac=fa0bbc98f60677c0a103e1293534af561a7a4298ebbe8fdd849869a04e9376d6";
    return ( 
        <div className="App" style={{backgroundImage: `url(${url})`, minHeight: "100vh"}}>
            <Navbar/>
            <main className="content">
                <Outlet/>
            </main> 
        </div>
    );
}
 
export default Root;