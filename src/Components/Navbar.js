import {Link, NavLink} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { jwtActions } from '../Store/indexStore';

const Navbar = () => {
    const dispatch = useDispatch();
    dispatch(jwtActions.tokenVerify());
    const tokenVerify = useSelector(state => state.jwt.tokenVerify);
    dispatch(jwtActions.tokenDecodeName());
    const name = useSelector(state => state.jwt.name);

    const logoutUser = () => {
        localStorage.clear();
        window.location.reload();
        
    }
    return (
        
        <nav className="navbar">
            <h1>The Blog</h1>
            <div className="links">
                <Link>{name}</Link>
                <NavLink to="/" className={({isActive}) => (isActive ? "aActive" : undefined )}>Home</NavLink>
                <NavLink to="/blog" className={({isActive}) => (isActive ? "aActive" : undefined )}>Blogs</NavLink>
                {tokenVerify && <NavLink to="/create" className={({isActive}) => (isActive ? "aActive" : undefined )}>New Blog</NavLink>} 
                {!tokenVerify && <NavLink to="/loginsignup" className={({isActive}) => (isActive ? "aActive" : undefined )}>Login/Signup</NavLink>}
                {tokenVerify && <button onClick={logoutUser}>Logout</button>}
            </div>
        </nav>
    );
}
 
export default Navbar;