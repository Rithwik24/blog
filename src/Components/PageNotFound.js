import { Link } from 'react-router-dom'

const PageNotFound = () => {
    return ( 
        <div className="page-not-found">
            <h2>Sorry! Page not found</h2>
            <Link to='/'>Back to HomePage</Link>
        </div>
     );
}
 
export default PageNotFound;