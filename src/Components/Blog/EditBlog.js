import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import useFetch from "../../useFetch";
import { useNavigate } from "react-router-dom";

const EditBlog = () => {
    const blogId = useSelector(state => state.ui.id);
    const [id, setId] = useState('');
    const [changeTitle, setChangeTitle] = useState('');
    const [changeContent, setChangeContent] = useState('');
    const [changeAuthor, setChangeAuthor] = useState('');
    const navigate = useNavigate();
    useEffect(() => {
        setId(blogId);
    },[blogId]);
    const {data: blog, loading, error} = useFetch('http://localhost:8000/blogs/'+id);

    useEffect(() => {
        console.log(blog);
        setChangeTitle(blog.title);
        setChangeContent(blog.content);
        setChangeAuthor(blog.author);
    },[blog]);
    const formSubmit = (e) =>{
        const blog = { title: changeTitle, content: changeContent, author: changeAuthor};

        fetch('http://localhost:8000/blogs/'+id, {
            method: 'PATCH',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(blog)
        })
        navigate(`/blog/${id}`);
    }
    return (  
        <div className="create">
            {loading && <div style={{textAlign:"center", marginTop:300, fontSize:30, textShadow:"0 0 1em black"}}>Loading Blog Content...</div>}
            {error && <div>{error}</div>}
            {blog &&
            <form onSubmit={formSubmit}>
                <label>Blog Title: </label>
                <input type="text" required value={changeTitle} onChange={(e) => setChangeTitle(e.target.value)}/>
                <label>Blog Content: </label>
                <textarea required value ={changeContent} onChange={(e) => setChangeContent(e.target.value)}/>
                <label>Blog Author:</label>
                <input type="text" readOnly value={changeAuthor} />
                <button>Save</button>
            </form>}
        </div>
    );
}
 
export default EditBlog;