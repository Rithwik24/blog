import BlogList from "./BlogList";
import useFetch from "../../useFetch";
import { useEffect, useState } from "react";

const Blog = ({author}) => {;
   const {data: blogs, loading, error } = useFetch('http://localhost:8000/blogs');
   const blog = Object.values(blogs);
   const [show, setShow] = useState(false);
   console.log(blogs);
//    const [auth, setAuth] = useState(auth);

   useEffect(() => {
        if(author === 'All Blogs!' || author === ''){
                setShow(true);
        }
        else{
            // author = author + "'s blog";
            setShow(false);
        }
   },[author]);

    return (  
        <div>
            {error && <div>{error}</div>}
            {blogs && show ?  <BlogList blogs={blogs} title="All Blogs!" /> : <BlogList blogs={blog.filter((blog) => blog.author === author)} title={author} />}
            {loading && <div style={{textAlign:"center", marginTop:300, fontSize:30, textShadow:"0 0 1em black"}}>Loading Blogs...</div>}
        </div>
    );
}
 
export default Blog;