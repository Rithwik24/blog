import { useEffect, useState } from "react";
import Blog from "./Blog";

const AuthorSelect = () => {
    const [blogs, setBlogs] = useState([]);
    // const dropdown = useSelector(state => state.ui.dropdown);
    const [options, setOptions] = useState('');

    useEffect(() => {
        fetch('http://localhost:8000/blogs').then((res) => res.json()).then((data)=>setBlogs(data))
    },[]);
    const uniqueBlogs = Array.from(new Set(blogs.map((blog) => blog.author))).map((author) => {
        return blogs.find((blog) => blog.author === author);
    });

    return ( 
        <>
        <div className="author">
            <select onChange={(e) => {setOptions(e.target.value);}}>
                {/* <option value={"none"}>--select Author--</option> */}
                <option value={"All Blogs!"}>All Blogs!</option>
                {
                    uniqueBlogs.map((option) => <option key={option.id} value={option.author}>{option.userId}</option>)
                }
            </select>
        </div> 
        <Blog author={options}/>
        </>
    );
}
 
export default AuthorSelect;