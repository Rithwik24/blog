import {useParams} from 'react-router-dom'
import useFetch from '../../useFetch';
import {useNavigate} from 'react-router-dom';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { jwtActions, uiActions } from '../../Store/indexStore';
import { Link } from 'react-router-dom';

const BlogsDetails = () => {
    const [enableButton, setEnableButton] = useState(false);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const userId = useSelector(state => state.jwt.id);
    dispatch(jwtActions.tokenDecodeId());
    const {id} = useParams();
    const {data: blog, loading, error} = useFetch('http://localhost:8000/blogs/'+id)
    
    const deleteBlog = () => {
        fetch('http://localhost:8000/blogs/' + blog.id, {
            method: 'DELETE'
        }).then(() => {
            navigate('/');
        })
    }
    const editBlog = () => {
        dispatch(uiActions.editBlog(id));
        navigate(`/blog/${id}/edit`);
        // console.log(id);
    }
    setTimeout(() => {
        if(blog.userId === userId){
            setEnableButton(true);
        } else {
            setEnableButton(false);
        }
    }, 2000);
    
    

    return (  
        <>
        <div className="blog-content">
            {loading && <div style={{textAlign:"center", marginTop:300, fontSize:30, textShadow:"0 0 1em black"}}>Loading Blog Content...</div>}
            {error && <div>{error}</div>}
            {blog &&
                <article>
                    <h2>{blog.title}</h2>
                    <p>Written by {blog.author}</p>
                    <div>{blog.content}</div>
                    {enableButton && <button onClick={deleteBlog}>Delete Blog</button>}
                    {enableButton && <button onClick={editBlog}>Edit Blog</button>}
                </article>}
        </div>
        {blog && <Link to=".." relative="path">Go back by clicking here.</Link>}
        </>
    );
}
 
export default BlogsDetails;