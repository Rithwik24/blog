import { useState } from "react";
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { jwtActions } from '../../Store/indexStore';

const Create = () => {
    const dispatch = useDispatch();
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    // const [file, setFile] = useState(null);
    const navigate = useNavigate();
    dispatch(jwtActions.tokenDecodeName());
    const name = useSelector(state => state.jwt.name);
    const id = useSelector(state => state.jwt.id);
    dispatch(jwtActions.tokenDecodeId());

    const formSubmit = (e) => {
        e.preventDefault();
        const blog = { title, content, author: name, userId: id};

        fetch('http://localhost:8000/blogs', {
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(blog)
        }).then(() => {
            navigate("/blog");
        })
    }

    return (  
        <div className="create">
            <h2>Add a new blog</h2>
            <form onSubmit={formSubmit}>
                <label>Blog Title: </label>
                <input type="text" required value={title} onChange={(e) => setTitle(e.target.value)}/>
                {/* <label>images/Files: </label>
                <input type="file" onChange={handleurl}/> */}
                <label>Blog Content: </label>
                <textarea required value ={content} onChange={(e) => setContent(e.target.value)}/>
                <label>Blog Author:</label>
                <input type="text" readOnly value={name} />
                <button>Add Blog</button>
            </form>
            {/* <image src={file} /> */}
        </div>
    );
}
 
export default Create;