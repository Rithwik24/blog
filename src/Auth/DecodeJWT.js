import * as jose from 'jose'
import {decrypt} from 'n-krypta';

const DecodeJWT = () => {
    const secretkey = 'ovvykUwuRd4k**q!A%L^!rA@nJ#3SpK3pGG4NEp7pF5*y3R^Zh';
    const token = localStorage.getItem("token");
    const decryptToken = decrypt(token, secretkey);
    const decodedToken = jose.decodeJwt(decryptToken);
    const name = decodedToken.name;
    const id = decodedToken.id;

    
    return {name, id};
}
 
export default DecodeJWT;