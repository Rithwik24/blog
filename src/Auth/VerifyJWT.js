import * as jose from 'jose'
import { useState } from 'react';

const VerifyJWT = (token) => {
    const [msg, setMSG] = useState(null);
    const [result, setResult] = useState(null);
    // setJWT(token);
    //console.log(token);

    const verifyJWT = async (token) => {
        const secret = new TextEncoder().encode(
            'cc7e0d44fd473002f1c42167459001140ec6389b7353f8088f4d9a95f2f596f2'
          );

        try{
        await jose.jwtVerify(token, secret, {
        issuer: 'urn:example:issuer',
        audience: 'urn:example:audience',
      });
      setMSG(true);
      return {msg};
    } catch(error){
        setMSG(false);
        return {msg};

    }

    }
    const output = verifyJWT(token);
        output.then(res => {
            setResult(res.msg);
        })

    return (result);
}
 
export default VerifyJWT;