import './index.css';
import Create from './Components/Blog/Create';
import { Navigate, RouterProvider, createBrowserRouter } from 'react-router-dom';
import { useEffect } from 'react';
import BlogsContent from './Components/Blog/BlogsContent';
import PageNotFound from './Components/PageNotFound';
import LoginPage from './Components/LoginSignup/LoginPage';
import SignUpPage from './Components/LoginSignup/SignUpPage';
import { useDispatch, useSelector } from 'react-redux';
import { jwtActions } from './Store/indexStore';
import Root from './Components/Root';
import LoginSignup from './Components/LoginSignup/LoginSignup';
import Home from './Components/Home';
import AuthorSelect from './Components/Blog/AuthorSelect';
import EditBlog from './Components/Blog/EditBlog';

function App() {
  const dispatch = useDispatch();
  const token = useSelector(state => state.jwt.token);
  const tokenVerify = useSelector(state => state.jwt.tokenVerify);
  dispatch(jwtActions.tokenVerify());
  const router = createBrowserRouter([
    {
      path: '/',
      element: <Root/>,
      children: [
        { index: true, element: <Home/> },
        { path: '/blog',element: <AuthorSelect/> },
        tokenVerify ? { path: '/create', element: <Create/> } : {path:'/create', element: <Navigate to ='/blog' replace />},
        tokenVerify ? { path: '/blog/:id/edit', element: <EditBlog/> } : {path:'/blog/:id/edit', element: <Navigate to=".." relative="path" replace />},
        !tokenVerify ? { path: '/loginsignup', element: <LoginSignup/> } : {path:'/loginsignup', element: <Navigate to ='/' replace />},
        !tokenVerify ? { path: '/loginsignup/login', element: <LoginPage/> } : {path:'/loginsignup/login', element: <Navigate to ='/' replace />},
        { path: '/blog/:id', element: <BlogsContent/> },
        !tokenVerify ? { path: '/loginsignup/signup', element: <SignUpPage/> } : {path:'/loginsignup/signup', element: <Navigate to ='/' replace />},
        { path: '*', element: <PageNotFound/> },
      ]
    }
  ])
  useEffect(() => {
    const handleStorageChange = (event) => {
      console.log(event.key);
      if (event.key !== token) {
        // Do something when 'yourLocalStorageKey' is edited
        localStorage.clear();
        window.location.reload();
        console.log('LocalStorage edited!');
      }
    };

    window.addEventListener('storage', handleStorageChange);

    return () => {
      window.removeEventListener('storage', handleStorageChange);
    };
  });
  
  return (
    <RouterProvider router={router} />
  );
}

export default App;