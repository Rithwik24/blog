import React, { Component } from 'react';
import { LoginContext } from './contexts/LoginContext';

class ThemeToggle extends Component {
  static contextType = LoginContext;
  render() { 
    const { toggleTheme } = this.context;
    return ( <button onClick={toggleTheme}>Toggle the theme</button>);
  }
}
 
export default ThemeToggle;