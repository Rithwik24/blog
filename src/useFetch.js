import { useEffect, useState } from "react";

const useFetch = (url) => {
    const [data, setData] = useState('');
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    
    useEffect(() => {
        const abortControl = new AbortController();

         setTimeout(() => {
            fetch(url, {signal: abortControl.signal})
            .then(res => {
                if(!res.ok){
                    throw Error('Error in loading blogs from server');
                }
                return res.json();
            })
            .then(data => {
                setData(data);
                setLoading(false);
                setError(null);
            })
            .catch(err => {
                setError(err.message);
                setLoading(false);
            })
           }, 2000);

          return () => abortControl.abort();
    },[url]);

    return {data, loading, error}
}
 
export default useFetch;