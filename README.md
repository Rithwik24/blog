<h1>Blog Website with User Authentication and Blog Management</h1>

<h3>Overview</h3>
This blog website is built using React.js and provides registered users with secure authentication using encrypted tokens stored in local storage. Users can create, edit, and delete their blogs, while guest users can view and sign up for these features. The project leverages the following technologies and libraries:

<ul type="circle">
<li>React.js</li>
<li>N-Krypta for token encryption</li>
<li>React Redux for state management</li>
<li>React Router DOM for routing</li>
<li>JSON Server for efficient data storage and retrieval</li>
</ul>

<h3>Features</h3>
<ul type="circle">
<li>User Registration: Users can create an account with a unique username and password.</li>
<li>User Authentication: Registered users can securely log in using encrypted tokens stored in local storage.</li>
<li>Blog Management: Users can create, edit, and delete their blogs.</li>
<li>Guest Users: Visitors to the website can view blogs and sign up for an account to access additional features.</li>
<li>Data Storage: JSON Server is used to efficiently store and retrieve blog data.</li>
</ul>

<h3>Installation</h3>
To run the project locally, follow these steps:
<ol type="1">
<li>Clone the repository to your local machine:

`git clone https://gitlab.com/rithwik024/blog.git`</li>

<li>Navigate to the project directory: 

`cd blog`</li>

<li>Install dependencies:
<ul type="circle">
<li>npm install</li>
<li>npm install react</li>
<li>npm install n-krypta</li>
<li>npm install react-redux</li>
<li>npm install --save react-router-dom</li>
</ul>

<li>Start the JSON Server for data storage:

`json-server --watch data/db.json --port 8000`</li>

<li>Start the React application:

`npm start`</li>

<li>Open your web browser and access the website at http://localhost:3000.</li>
</ol>

<h3>Usage</h3>
<ul type="circle">
<li><b>Register:</b> Create a new account with a unique username and password.</li>
<li><b>Login:</b> Log in securely using your registered credentials.</li>
<li><b>View Blogs:</b> Browse and read blogs created by other users.</li>
<li><b>Create a Blog:</b> Registered users can create their blogs.</li>
<li><b>Edit and Delete Blogs:</b> Registered users can manage their blogs by editing or deleting them.</li>
</ul>

<h3>Technologies and Libraries</h3>
<ul type="circle">
<li><b>React.js:</b> The frontend framework used to build the user interface and manage user interactions.</li>
<li><b>N-Krypta:</b> N-Krypta is used for token encryption to ensure secure user authentication.</li>
<li><b>React Redux:</b> State management library for managing application state, user data, and blog information.</li>
<li><b>React Router DOM:</b> Routing library used for navigating between different parts of the application.</li>
<li><b>JSON Server:</b> A lightweight, efficient, and full-fledged fake REST API server for storing and retrieving blog data.</li>
